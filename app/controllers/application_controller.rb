class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :set_locale

  private

  def extract_locale_from_accept_language_header # pone el idioma automaticamente al usuario segun lo q diga su browser
    request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
  end

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
    Rails.application.routes.default_url_options[:locale]= I18n.locale
  end

  #mirar abajo de /config/environments/development.rb que es donde se configura el idioma.






end
