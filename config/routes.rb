Ejemplo2I18n::Application.routes.draw do

  scope "(:locale)", :locale => /en|de/ do
    root :to => 'products#index'
    resources :products
  end


end
